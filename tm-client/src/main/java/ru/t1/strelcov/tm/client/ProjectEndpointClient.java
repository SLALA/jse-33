package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public final class ProjectEndpointClient extends AbstractEndpointClient implements IProjectEndpoint {

    public ProjectEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @NotNull
    @Override
    public ProjectListResponse listProject(@NotNull ProjectListRequest request) {
        return call(request, ProjectListResponse.class);
    }

    @NotNull
    @Override
    public ProjectListSortedResponse listSortedProject(@NotNull ProjectListSortedRequest request) {
        return call(request, ProjectListSortedResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByIdResponse changeStatusByIdProject(@NotNull ProjectChangeStatusByIdRequest request) {
        return call(request, ProjectChangeStatusByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectChangeStatusByNameResponse changeStatusByNameProject(@NotNull ProjectChangeStatusByNameRequest request) {
        return call(request, ProjectChangeStatusByNameResponse.class);
    }

    @NotNull
    @Override
    public ProjectClearResponse clearProject(@NotNull ProjectClearRequest request) {
        return call(request, ProjectClearResponse.class);
    }

    @NotNull
    @Override
    public ProjectCreateResponse createProject(@NotNull ProjectCreateRequest request) {
        return call(request, ProjectCreateResponse.class);
    }

    @NotNull
    @Override
    public ProjectFindByIdResponse findByIdProject(@NotNull ProjectFindByIdRequest request) {
        return call(request, ProjectFindByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectFindByNameResponse findByNameProject(@NotNull ProjectFindByNameRequest request) {
        return call(request, ProjectFindByNameResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByIdResponse removeByIdProject(@NotNull ProjectRemoveByIdRequest request) {
        return call(request, ProjectRemoveByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveByNameResponse removeByNameProject(@NotNull ProjectRemoveByNameRequest request) {
        return call(request, ProjectRemoveByNameResponse.class);
    }

    @NotNull
    @Override
    public ProjectRemoveWithTasksResponse removeWithTasksProject(@NotNull ProjectRemoveWithTasksRequest request) {
        return call(request, ProjectRemoveWithTasksResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByIdResponse updateByIdProject(@NotNull ProjectUpdateByIdRequest request) {
        return call(request, ProjectUpdateByIdResponse.class);
    }

    @NotNull
    @Override
    public ProjectUpdateByNameResponse updateByNameProject(@NotNull ProjectUpdateByNameRequest request) {
        return call(request, ProjectUpdateByNameResponse.class);
    }

}
