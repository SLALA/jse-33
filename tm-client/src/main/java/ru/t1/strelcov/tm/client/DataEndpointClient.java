package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IDataEndpoint;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import java.net.Socket;

@NoArgsConstructor
public final class DataEndpointClient extends AbstractEndpointClient implements IDataEndpoint {

    public DataEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @NotNull
    @Override
    public DataBase64LoadResponse loadBase64Data(@NotNull DataBase64LoadRequest request) {
        return call(request, DataBase64LoadResponse.class);
    }

    @NotNull
    @Override
    public DataBase64SaveResponse saveBase64Data(@NotNull DataBase64SaveRequest request) {
        return call(request, DataBase64SaveResponse.class);
    }

    @NotNull
    @Override
    public DataBinaryLoadResponse loadBinaryData(@NotNull DataBinaryLoadRequest request) {
        return call(request, DataBinaryLoadResponse.class);
    }

    @NotNull
    @Override
    public DataBinarySaveResponse saveBinaryData(@NotNull DataBinarySaveRequest request) {
        return call(request, DataBinarySaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonJAXBSaveResponse saveJsonJAXBData(@NotNull DataJsonJAXBSaveRequest request) {
        return call(request, DataJsonJAXBSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonJAXBLoadResponse loadJsonJAXBData(@NotNull DataJsonJAXBLoadRequest request) {
        return call(request, DataJsonJAXBLoadResponse.class);
    }

    @NotNull
    @Override
    public DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(@NotNull DataJsonFasterXMLLoadRequest request) {
        return call(request, DataJsonFasterXMLLoadResponse.class);
    }

    @NotNull
    @Override
    public DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(@NotNull DataXmlFasterXMLLoadRequest request) {
        return call(request, DataXmlFasterXMLLoadResponse.class);
    }

    @NotNull
    @Override
    public DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(@NotNull DataYamlFasterXMLSaveRequest request) {
        return call(request, DataYamlFasterXMLSaveResponse.class);
    }

    @NotNull
    @Override
    public DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(@NotNull DataJsonFasterXMLSaveRequest request) {
        return call(request, DataJsonFasterXMLSaveResponse.class);
    }

    @NotNull
    @Override
    public DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(@NotNull DataXmlFasterXMLSaveRequest request) {
        return call(request, DataXmlFasterXMLSaveResponse.class);
    }

    @NotNull
    @Override
    public DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(@NotNull DataYamlFasterXMLLoadRequest request) {
        return call(request, DataYamlFasterXMLLoadResponse.class);
    }

    @NotNull
    @Override
    public DataXmlJAXBLoadResponse loadXmlJAXBData(@NotNull DataXmlJAXBLoadRequest request) {
        return call(request, DataXmlJAXBLoadResponse.class);
    }

    @NotNull
    @Override
    public DataXmlJAXBSaveResponse saveXmlJAXBData(@NotNull DataXmlJAXBSaveRequest request) {
        return call(request, DataXmlJAXBSaveResponse.class);
    }

}
