package ru.t1.strelcov.tm.client;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;

import java.net.Socket;

@NoArgsConstructor
public final class AuthEndpointClient extends AbstractEndpointClient implements IAuthEndpoint {

    public AuthEndpointClient(@Nullable final Socket socket) {
        super(socket);
    }

    @NotNull
    @Override
    public UserLoginResponse login(@NotNull UserLoginRequest request) {
        return call(request, UserLoginResponse.class);
    }

    @NotNull
    @Override
    public UserLogoutResponse logout(@NotNull UserLogoutRequest request) {
        return call(request, UserLogoutResponse.class);
    }

    @NotNull
    @Override
    public UserProfileResponse getProfile(@NotNull UserProfileRequest request) {
        return call(request, UserProfileResponse.class);
    }

}
