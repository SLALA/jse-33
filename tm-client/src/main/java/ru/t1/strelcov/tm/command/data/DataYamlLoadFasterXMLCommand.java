package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataYamlFasterXMLLoadRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataYamlLoadFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-yaml-load-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Load entities data from yaml file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA YAML LOAD]");
        serviceLocator.getDataEndpoint().loadYamlFasterXMLData(new DataYamlFasterXMLLoadRequest());
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
