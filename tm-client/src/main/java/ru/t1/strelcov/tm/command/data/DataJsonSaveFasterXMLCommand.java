package ru.t1.strelcov.tm.command.data;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.dto.request.DataJsonFasterXMLSaveRequest;
import ru.t1.strelcov.tm.enumerated.Role;

public class DataJsonSaveFasterXMLCommand extends AbstractDataCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @NotNull
    public String name() {
        return "data-json-save-fasterxml";
    }

    @Override
    @NotNull
    public String description() {
        return "Save entities data to json file.";
    }

    @Override
    @SneakyThrows
    public void execute() {
        System.out.println("[DATA JSON SAVE]");
        serviceLocator.getDataEndpoint().saveJsonFasterXMLData(new DataJsonFasterXMLSaveRequest());
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{
                Role.ADMIN
        };
    }

}
