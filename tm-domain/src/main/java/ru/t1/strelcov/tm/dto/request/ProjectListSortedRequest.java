package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class ProjectListSortedRequest extends AbstractUserRequest {

    @Nullable
    private String sort;

    public ProjectListSortedRequest(@Nullable final String sort) {
        this.sort = sort;
    }

}
