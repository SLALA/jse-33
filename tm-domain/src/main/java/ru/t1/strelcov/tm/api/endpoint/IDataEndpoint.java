package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDataEndpoint {

    @WebMethod
    @NotNull
    DataBase64LoadResponse loadBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request);

    @WebMethod
    @NotNull
    DataBase64SaveResponse saveBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request);

    @WebMethod
    @NotNull
    DataBinaryLoadResponse loadBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request);

    @WebMethod
    @NotNull
    DataBinarySaveResponse saveBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request);

    @WebMethod
    @NotNull
    DataJsonJAXBSaveResponse saveJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBSaveRequest request);

    @WebMethod
    @NotNull
    DataJsonJAXBLoadResponse loadJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBLoadRequest request);

    @WebMethod
    @NotNull
    DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLSaveRequest request);

    @WebMethod
    @NotNull
    DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlJAXBLoadResponse loadXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBLoadRequest request);

    @WebMethod
    @NotNull
    DataXmlJAXBSaveResponse saveXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBSaveRequest request);

}
