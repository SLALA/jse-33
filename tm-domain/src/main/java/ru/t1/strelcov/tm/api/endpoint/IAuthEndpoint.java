package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IAuthEndpoint {

    @WebMethod
    @NotNull
    UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLoginRequest request);

    @WebMethod
    @NotNull
    UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLogoutRequest request);

    @WebMethod
    @NotNull
    UserProfileResponse getProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull UserProfileRequest request);

}
