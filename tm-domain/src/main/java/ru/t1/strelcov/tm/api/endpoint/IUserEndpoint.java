package ru.t1.strelcov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IUserEndpoint {

    @WebMethod
    @NotNull
    UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull UserChangePasswordRequest request);

    @WebMethod
    @NotNull
    UserListResponse listUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserListRequest request);

    @WebMethod
    @NotNull
    UserRegisterResponse registerUser(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRegisterRequest request);

    @WebMethod
    @NotNull
    UserRemoveByLoginResponse removeUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserRemoveByLoginRequest request);

    @WebMethod
    @NotNull
    UserUpdateByLoginResponse updateUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUpdateByLoginRequest request);

    @WebMethod
    @NotNull
    UserUnlockByLoginResponse unlockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserUnlockByLoginRequest request);

    @WebMethod
    @NotNull
    UserLockByLoginResponse lockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull UserLockByLoginRequest request);

}
