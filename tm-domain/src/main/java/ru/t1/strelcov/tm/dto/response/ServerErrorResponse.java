package ru.t1.strelcov.tm.dto.response;

import org.jetbrains.annotations.NotNull;


public final class ServerErrorResponse extends AbstractResultResponse {

    public ServerErrorResponse() {
        super();
        setSuccess(false);
    }

    public ServerErrorResponse(@NotNull Throwable throwable) {
        super(throwable);
    }

}
