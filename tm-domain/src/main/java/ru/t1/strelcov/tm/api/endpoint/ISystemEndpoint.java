package ru.t1.strelcov.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.dto.request.ServerAboutRequest;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.dto.response.ServerAboutResponse;
import ru.t1.strelcov.tm.dto.response.ServerVersionResponse;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import java.net.URL;
import java.util.concurrent.RecursiveTask;

@WebService
public interface ISystemEndpoint {

    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance() {
        return newInstance("localhost", 8080);
    }

    @SneakyThrows
    @WebMethod(exclude = true)
    static ISystemEndpoint newInstance(@NotNull final String host, @NotNull final Integer port) {
        @NotNull final String wsdlURL = "http://" + host + ":" + port + "/" + "SystemEndpoint" + "?wsdl";
        @NotNull final URL url = new URL(wsdlURL);
        @NotNull final QName qName = new QName("http://endpoint.tm.strelcov.t1.ru/", "SystemEndpointService");
        return Service.create(url, qName).getPort(ISystemEndpoint.class);
    }

    @WebMethod
    @NotNull
    ServerAboutResponse getAbout(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerAboutRequest request);

    @WebMethod
    @NotNull
    ServerVersionResponse getVersion(
            @WebParam(name = "request", partName = "request")
            @NotNull ServerVersionRequest request);

}
