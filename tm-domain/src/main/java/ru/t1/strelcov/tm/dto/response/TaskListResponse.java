package ru.t1.strelcov.tm.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.model.Task;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public final class TaskListResponse extends AbstractResultResponse {

    @NotNull
    private List<Task> list;

    public TaskListResponse(@NotNull List<Task> list) {
        this.list = list;
    }

}
