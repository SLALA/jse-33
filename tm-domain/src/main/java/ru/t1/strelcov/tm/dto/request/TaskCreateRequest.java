package ru.t1.strelcov.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@NoArgsConstructor
@Getter
@Setter
public final class TaskCreateRequest extends AbstractUserRequest {

    @Nullable
    private String description;

    @Nullable
    private String name;

    public TaskCreateRequest(@Nullable final String name, @Nullable final String description) {
        this.name = name;
        this.description = description;
    }

}
