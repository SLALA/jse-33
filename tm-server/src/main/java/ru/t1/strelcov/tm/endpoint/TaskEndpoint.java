package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint;
import ru.t1.strelcov.tm.api.service.ITaskService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;

import java.util.Comparator;
import java.util.Optional;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.ITaskEndpoint")
@NoArgsConstructor
public final class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private ITaskService getTaskService() {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getTaskService();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListResponse listTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListRequest request) {
        check(request);
        return new TaskListResponse(getTaskService().findAll(request.getUserId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListSortedResponse listSortedTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListSortedRequest request) {
        check(request);
        final String sort = request.getSort();
        if (SortType.isValidByName(sort)) {
            @NotNull final SortType sortType = SortType.valueOf(sort);
            @NotNull final Comparator comparator = sortType.getComparator();
            return new TaskListSortedResponse(getTaskService().findAll(request.getUserId(), comparator));
        } else
            throw new IncorrectSortOptionException(sort);
    }

    @NotNull
    @Override
    @WebMethod
    public TaskListByProjectIdResponse listByProjectIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskListByProjectIdRequest request) {
        check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskListByProjectIdResponse(serviceLocator.getProjectTaskService().findAllTasksByProjectId(request.getUserId(), request.getProjectId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByIdResponse changeStatusByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskChangeStatusByIdRequest request) {
        check(request);
        return new TaskChangeStatusByIdResponse(getTaskService().changeStatusById(request.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskChangeStatusByNameResponse changeStatusByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskChangeStatusByNameRequest request) {
        check(request);
        return new TaskChangeStatusByNameResponse(getTaskService().changeStatusByName(request.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskClearResponse clearTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskClearRequest request) {
        check(request);
        getTaskService().clear(request.getUserId());
        return new TaskClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public TaskCreateResponse createTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskCreateRequest request) {
        check(request);
        return new TaskCreateResponse(getTaskService().add(request.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskFindByIdResponse findByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskFindByIdRequest request) {
        check(request);
        return new TaskFindByIdResponse(getTaskService().findById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskFindByNameResponse findByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskFindByNameRequest request) {
        check(request);
        return new TaskFindByNameResponse(getTaskService().findByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByIdResponse removeByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskRemoveByIdRequest request) {
        check(request);
        return new TaskRemoveByIdResponse(getTaskService().removeById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskRemoveByNameResponse removeByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskRemoveByNameRequest request) {
        check(request);
        return new TaskRemoveByNameResponse(getTaskService().removeByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskBindToProjectResponse bindTaskToProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskBindToProjectRequest request) {
        check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskBindToProjectResponse(serviceLocator.getProjectTaskService().bindTaskToProject(request.getUserId(), request.getTaskId(), request.getProjectId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUnbindFromProjectResponse unbindTaskFromProject(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUnbindFromProjectRequest request) {
        check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new TaskUnbindFromProjectResponse(serviceLocator.getProjectTaskService().unbindTaskFromProject(request.getUserId(), request.getTaskId()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByIdResponse updateByIdTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUpdateByIdRequest request) {
        check(request);
        return new TaskUpdateByIdResponse(getTaskService().updateById(request.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    @WebMethod
    public TaskUpdateByNameResponse updateByNameTask(
            @WebParam(name = "request", partName = "request")
            @NotNull TaskUpdateByNameRequest request) {
        check(request);
        return new TaskUpdateByNameResponse(getTaskService().updateById(request.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }

}
