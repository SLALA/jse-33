package ru.t1.strelcov.tm.util;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class SystemUtil {

    public static long getPID() {
        final String processName = java.lang.management.ManagementFactory.getRuntimeMXBean().getName();
        if (processName != null && processName.length() > 0) {
            try {
                return Long.parseLong(processName.split("@")[0]);
            } catch (Exception e) {
                return 0;
            }
        }
        return 0;
    }

}
