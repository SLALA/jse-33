package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint;
import ru.t1.strelcov.tm.api.service.IProjectService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.SortType;
import ru.t1.strelcov.tm.enumerated.Status;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.system.IncorrectSortOptionException;

import java.util.Comparator;
import java.util.Optional;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IProjectEndpoint")
@NoArgsConstructor
public final class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IProjectService getProjectService() {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getProjectService();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListResponse listProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListRequest request) {
        check(request);
        return new ProjectListResponse(getProjectService().findAll(request.getUserId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectListSortedResponse listSortedProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectListSortedRequest request) {
        check(request);
        final String sort = request.getSort();
        if (SortType.isValidByName(sort)) {
            @NotNull final SortType sortType = SortType.valueOf(sort);
            @NotNull final Comparator comparator = sortType.getComparator();
            return new ProjectListSortedResponse(getProjectService().findAll(request.getUserId(), comparator));
        } else
            throw new IncorrectSortOptionException(sort);
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByIdResponse changeStatusByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByIdRequest request) {
        check(request);
        return new ProjectChangeStatusByIdResponse(getProjectService().changeStatusById(request.getUserId(), request.getId(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectChangeStatusByNameResponse changeStatusByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectChangeStatusByNameRequest request) {
        check(request);
        return new ProjectChangeStatusByNameResponse(getProjectService().changeStatusByName(request.getUserId(), request.getName(), Status.valueOf(request.getStatus())));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectClearResponse clearProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectClearRequest request) {
        check(request);
        getProjectService().clear(request.getUserId());
        return new ProjectClearResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectCreateResponse createProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectCreateRequest request) {
        check(request);
        return new ProjectCreateResponse(getProjectService().add(request.getUserId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectFindByIdResponse findByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectFindByIdRequest request) {
        check(request);
        return new ProjectFindByIdResponse(getProjectService().findById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectFindByNameResponse findByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectFindByNameRequest request) {
        check(request);
        return new ProjectFindByNameResponse(getProjectService().findByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByIdResponse removeByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByIdRequest request) {
        check(request);
        return new ProjectRemoveByIdResponse(getProjectService().removeById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveByNameResponse removeByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveByNameRequest request) {
        check(request);
        return new ProjectRemoveByNameResponse(getProjectService().removeByName(request.getUserId(), request.getName()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectRemoveWithTasksResponse removeWithTasksProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectRemoveWithTasksRequest request) {
        check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return new ProjectRemoveWithTasksResponse(serviceLocator.getProjectTaskService().removeProjectById(request.getUserId(), request.getId()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByIdResponse updateByIdProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByIdRequest request) {
        check(request);
        return new ProjectUpdateByIdResponse(getProjectService().updateById(request.getUserId(), request.getId(), request.getName(), request.getDescription()));
    }

    @NotNull
    @Override
    @WebMethod
    public ProjectUpdateByNameResponse updateByNameProject(
            @WebParam(name = "request", partName = "request")
            @NotNull ProjectUpdateByNameRequest request) {
        check(request);
        return new ProjectUpdateByNameResponse(getProjectService().updateById(request.getUserId(), request.getOldName(), request.getName(), request.getDescription()));
    }
}
