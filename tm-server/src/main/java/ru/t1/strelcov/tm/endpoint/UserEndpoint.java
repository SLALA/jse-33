package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IUserEndpoint;
import ru.t1.strelcov.tm.api.service.IUserService;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.model.User;

import java.util.List;
import java.util.Optional;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IUserEndpoint")
@NoArgsConstructor
public final class UserEndpoint extends AbstractEndpoint implements IUserEndpoint {

    public UserEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    private IUserService getUserService() {
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        return serviceLocator.getUserService();
    }

    @Override
    @NotNull
    public UserChangePasswordResponse changePassword(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserChangePasswordRequest request) {
        check(request);
        getUserService().changePasswordById(request.getUserId(), request.getPassword());
        return new UserChangePasswordResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserListResponse listUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserListRequest request) {
        check(request, Role.ADMIN);
        @NotNull List<User> list = getUserService().findAll();
        return new UserListResponse(list);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRegisterResponse registerUser(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserRegisterRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().add(request.getLogin(), request.getPassword(), request.getEmail());
        return new UserRegisterResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserRemoveByLoginResponse removeUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserRemoveByLoginRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().removeByLogin(request.getLogin());
        return new UserRemoveByLoginResponse(user);
    }

    @NotNull
    @Override
    @WebMethod
    public UserUpdateByLoginResponse updateUserByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUpdateByLoginRequest request) {
        check(request, Role.ADMIN);
        @NotNull User user = getUserService().updateByLogin(request.getLogin(), request.getFirstName(), request.getLastName(), null, request.getEmail());
        return new UserUpdateByLoginResponse(user);
    }


    @NotNull
    @Override
    @WebMethod
    public UserUnlockByLoginResponse unlockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserUnlockByLoginRequest request) {
        check(request, Role.ADMIN);
        getUserService().unlockUserByLogin(request.getLogin());
        return new UserUnlockByLoginResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLockByLoginResponse lockByLogin(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLockByLoginRequest request) {
        check(request, Role.ADMIN);
        getUserService().lockUserByLogin(request.getLogin());
        return new UserLockByLoginResponse();
    }

}
