package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.AbstractUserRequest;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.exception.entity.UserLockedException;
import ru.t1.strelcov.tm.model.User;

import java.util.Optional;

@NoArgsConstructor
public abstract class AbstractEndpoint {

    @Nullable
    protected ServiceLocator serviceLocator;

    public AbstractEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    protected void check(
            @NotNull AbstractUserRequest request) {
        Optional.ofNullable(request.getUserId()).filter((i) -> !i.isEmpty()).orElseThrow(AccessDeniedException::new);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        @NotNull final User user = serviceLocator.getUserService().findById(request.getUserId());
        if (user.getLocked()) throw new UserLockedException();
    }

    protected void check(
            @NotNull AbstractUserRequest request, @Nullable Role... roles) {
        check(request);
        if (roles == null || roles.length == 0) return;
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        @NotNull final Role userRole = Optional.of(serviceLocator.getUserService().findById(request.getUserId()))
                .map(User::getRole).orElseThrow(AccessDeniedException::new);
        for (final Role role : roles)
            if (userRole.equals(role)) return;
        throw new AccessDeniedException();
    }

}
