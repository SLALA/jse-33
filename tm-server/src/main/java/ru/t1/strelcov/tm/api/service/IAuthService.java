package ru.t1.strelcov.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.t1.strelcov.tm.enumerated.Role;
import ru.t1.strelcov.tm.model.User;

public interface IAuthService {

    User check(@Nullable String login, @Nullable String password);

    void checkRoles(@Nullable String userId, @Nullable Role... roles);

}
