package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.UserLoginRequest;
import ru.t1.strelcov.tm.dto.request.UserLogoutRequest;
import ru.t1.strelcov.tm.dto.request.UserProfileRequest;
import ru.t1.strelcov.tm.dto.response.UserLoginResponse;
import ru.t1.strelcov.tm.dto.response.UserLogoutResponse;
import ru.t1.strelcov.tm.dto.response.UserProfileResponse;
import ru.t1.strelcov.tm.exception.entity.AccessDeniedException;
import ru.t1.strelcov.tm.model.User;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Optional;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IAuthEndpoint")
@NoArgsConstructor
public final class AuthEndpoint extends AbstractEndpoint implements IAuthEndpoint {

    public AuthEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }


    @NotNull
    @Override
    @WebMethod
    public UserLoginResponse login(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLoginRequest request) {
        return new UserLoginResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserLogoutResponse logout(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserLogoutRequest request) {
        return new UserLogoutResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public UserProfileResponse getProfile(
            @WebParam(name = "request", partName = "request")
            @NotNull final UserProfileRequest request) {
        check(request);
        Optional.ofNullable(serviceLocator).orElseThrow(AccessDeniedException::new);
        @NotNull final User user = serviceLocator.getUserService().findById(request.getUserId());
        return new UserProfileResponse(user);
    }

}
