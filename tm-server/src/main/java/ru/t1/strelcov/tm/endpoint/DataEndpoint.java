package ru.t1.strelcov.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.endpoint.IDataEndpoint;
import ru.t1.strelcov.tm.api.service.ServiceLocator;
import ru.t1.strelcov.tm.dto.request.*;
import ru.t1.strelcov.tm.dto.response.*;
import ru.t1.strelcov.tm.enumerated.Role;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService(endpointInterface = "ru.t1.strelcov.tm.api.endpoint.IDataEndpoint")
@NoArgsConstructor
public final class DataEndpoint extends AbstractEndpoint implements IDataEndpoint {

    public DataEndpoint(@NotNull final ServiceLocator serviceLocator) {
        super(serviceLocator);
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64LoadResponse loadBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64LoadRequest request) {
        check(request, Role.ADMIN);
        return new DataBase64LoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBase64SaveResponse saveBase64Data(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBase64SaveRequest request) {
        check(request, Role.ADMIN);
        return new DataBase64SaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinaryLoadResponse loadBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinaryLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataBinaryLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataBinarySaveResponse saveBinaryData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataBinarySaveRequest request) {
        check(request, Role.ADMIN);
        return new DataBinarySaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJAXBSaveResponse saveJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonJAXBSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonJAXBLoadResponse loadJsonJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonJAXBLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonJAXBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXMLLoadResponse loadJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXMLLoadResponse loadXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXMLSaveResponse saveYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataYamlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataJsonFasterXMLSaveResponse saveJsonFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataJsonFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataJsonFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlFasterXMLSaveResponse saveXmlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlFasterXMLSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlFasterXMLSaveResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataYamlFasterXMLLoadResponse loadYamlFasterXMLData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataYamlFasterXMLLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataYamlFasterXMLLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJAXBLoadResponse loadXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBLoadRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlJAXBLoadResponse();
    }

    @NotNull
    @Override
    @WebMethod
    public DataXmlJAXBSaveResponse saveXmlJAXBData(
            @WebParam(name = "request", partName = "request")
            @NotNull DataXmlJAXBSaveRequest request) {
        check(request, Role.ADMIN);
        return new DataXmlJAXBSaveResponse();
    }

}
